$('.invest_tabs .tab').click(function() {
    $('.invest_tabs .tab').removeClass('active');
    $(this).addClass('active');
    var id = $(this).attr('id');
    $('.inv').fadeOut( 0, function() { });
    $('.inv.id_' + id).fadeIn( 600, function() { });
});

/* Копирование ссылки при клике на кнопку */

$('#copybtn').click(function () {
    copy();
});

function copy() {
    var cutTextarea = document.querySelector('#copy');
    cutTextarea.select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copy');
    } catch (err) {
        console.log('Oops, unable to cut');
    }
}

/* Инициализация WOW эффектов */

new WOW().init();

$(document).ready(function() {
    $("#ipad-screen").animate({
        height: 470
    }, 2000);
});